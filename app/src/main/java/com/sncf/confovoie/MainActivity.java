package com.sncf.confovoie;


import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView confovoiewebview = (WebView) findViewById(R.id.webview);
        confovoiewebview.getSettings().setLoadsImagesAutomatically(true);
        confovoiewebview.getSettings().setDomStorageEnabled(true);
        confovoiewebview.getSettings().setJavaScriptEnabled(true);
        confovoiewebview.getSettings().setAppCachePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/cache");
        confovoiewebview.getSettings().setDatabasePath(getApplicationContext().getFilesDir().getAbsolutePath() + "/databases");
        confovoiewebview.loadUrl("file:///android_asset/index.html");
        confovoiewebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
    }
}
